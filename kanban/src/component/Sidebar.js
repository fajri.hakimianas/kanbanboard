import React from 'react'

const Sidebar = () => {
    return (
        <>
            <div className="sidebar">
                <form className="form-inline">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn" type="submit">
                        <i className="bi bi-search"></i>
                    </button>
                </form>
                <div className="row mt-4 ml-2">
                    <div className="col-4">
                        <img src="https://cdn.dribbble.com/users/2313212/screenshots/11256142/media/27b57b3ee2ac221dc8c616d02161d96b.jpg?compress=1&resize=400x300" width="80" height="80" alt="" loading="lazy"/>
                    </div>
                    <div className="col-8">
                        <div className="name-profile">
                            <h6>Fajri Hakimi Anas</h6>
                            <p>Frontend Developer</p>
                        </div>
                    </div>
                </div>

                <div className="row mt-2 ml-2">
                    <div className="col-5">
                        <h6>372</h6>
                        <p>Completed Tasks</p>
                    </div>
                    <div className="col-4">
                        <h6>11</h6>
                        <p>Open Task</p>
                    </div>
                </div>
                <ul>
                    <h4>MENU</h4>
                    <a href="/">Home</a>
                    <a href="/">My Tasks</a>
                    <a href="/">Notifications</a>

                    <h4 className="mt-4">TEAMS</h4>
                    <a href="/">Researchers</a>
                    <a href="/">FE/BE Team</a>
                    <a href="/">PM Team</a>

                    <button className="btn" type="submit">
                        Add a Team
                    </button>
                </ul> 
            </div>
        </>
    )
}

export default Sidebar
